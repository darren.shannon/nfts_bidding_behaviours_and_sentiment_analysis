# NFTs_Bidding_Behaviours_and_Sentiment_Analysis
- This project serves as a repository for hosting the raw data and jupyter notebook files needed to recreate the analysis for _Dutch auction dynamics in non-fungible token (NFT) markets_.

- **Virtual environment / project generation / library / root files are not provided and will need to be created before running the Python code.**

- Minor code amendments are required in each notebook to open the data file and save generated images locally.

- Information on the library packages required to run the analysis are provided in the relevant notebooks.

- Please direct any detected errors in the code / data to: darren.shannon@ul.ie
